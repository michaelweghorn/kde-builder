# Changelog

2024-04-14
: Separated project-wide "Debug()" logger into separate loggers.

2024-03-27
: Added `source-when-start-program` option.

2024-02-28
: Released kde-builder. All the implementation and features are synced in both kdesrc-build and kde-builder.
This point is marked with the commit "MEGA RELEASE" in git history of both projects.
